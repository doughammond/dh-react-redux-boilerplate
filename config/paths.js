const path = require('path');

function resolve(relativePath) {
  return path.resolve(__dirname, relativePath);
}

module.exports = {
  appBuild: resolve('../build'),
  appHtml: resolve('../templates/index.html'),
  appFavicon: resolve('../assets/favicon.ico'),
  appPackageJson: resolve('../package.json'),
  appSrc: resolve('../src'),
  appConfig: resolve('../config'),
  appNodeModules: resolve('../node_modules'),
  ownNodeModules: resolve('../node_modules')
};
