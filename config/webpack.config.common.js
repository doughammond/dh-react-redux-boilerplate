// @flow

const path = require('path');
const paths = require('./paths');
const autoprefixer = require('autoprefixer');
const wpconfig = require('webpack-config');

module.exports = new wpconfig.Config().merge({
  resolve: {
    extensions: ['', '.js', '.json'],
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        include: [paths.appSrc, paths.appConfig],
      }
    ],
    loaders: [
      {
        test: /\.js(x)?$/,
        include: [paths.appSrc, paths.appConfig, paths.appNodeModules],
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: require('./babel.dev')
      },
      {
        test: /\.scss$/,
        include: paths.appSrc,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.json$/,
        include: [paths.appSrc, paths.appNodeModules],
        loader: 'json-loader'
      },
      {
        test: /\.(mp4|webm)$/,
        include: [paths.appSrc, paths.appNodeModules],
        loader: 'url-loader?limit=10000'
      }
    ]
  },
  eslint: {
    configFile: path.join(__dirname, 'eslint.js'),
    useEslintrc: false
  },
  postcss: function() {
    return [autoprefixer];
  },
  output: {
    path: paths.appBuild
  }
});
