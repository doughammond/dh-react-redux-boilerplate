// @flow

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');
const wpconfig = require('webpack-config');

module.exports = new wpconfig.Config()
  .extend('config/webpack.config.common')
  .merge({
    devtool: 'eval',
    entry: [
      require.resolve('webpack-dev-server/client'),
      require.resolve('webpack/hot/dev-server'),
      path.join(paths.appSrc, 'index')
    ],
    output: {
      pathinfo: true,
      filename: 'bundle.js',
      publicPath: '/',
    },
    module: {
      loaders: [
        {
          test: /\.css$/,
          include: [paths.appSrc, paths.appNodeModules],
          loader: 'style!css!postcss'
        },
        {
          test: /\.(jpg|png|gif|eot|svg|ttf|woff|woff2)$/,
          include: [paths.appSrc, paths.appNodeModules],
          loader: 'file',
        },
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
        favicon: paths.appFavicon,
      }),
      new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"development"' }),
      // Note: only CSS is currently hot reloaded :(
      new webpack.HotModuleReplacementPlugin()
    ]
  });
