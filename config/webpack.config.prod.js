const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const url = require('url');
const paths = require('./paths');

const homepagePath = require(paths.appPackageJson).homepage;
let publicPath = homepagePath ? url.parse(homepagePath).pathname : '/';
if (!publicPath.endsWith('/')) {
  // Prevents incorrect paths in file-loader
  publicPath += '/';
}

const wpconfig = require('webpack-config');

module.exports = new wpconfig.Config()
  .extend('config/webpack.config.common')
  .merge({
    bail: true,
    devtool: 'source-map',
    entry: [
      path.join(paths.appSrc, 'index')
    ],
    output: {
      filename: '[name].[chunkhash:8].js',
      chunkFilename: '[name].[chunkhash:8].chunk.js',
      publicPath: publicPath
    },
    module: {
      loaders: [
        {
          test: /\.css$/,
          include: [paths.appSrc, paths.appNodeModules],
          // Disable autoprefixer in css-loader itself:
          // https://github.com/webpack/css-loader/issues/281
          // We already have it thanks to postcss.
          loader: ExtractTextPlugin.extract('style', 'css?-autoprefixer!postcss')
        },
        {
          test: /\.(jpg|png|gif|eot|svg|ttf|woff|woff2)$/,
          include: [paths.appSrc, paths.appNodeModules],
          loader: 'file',
          query: {
            name: '[name].[hash:8].[ext]'
          }
        },
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
        favicon: paths.appFavicon,
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true
        }
      }),
      new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"production"' }),
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          screw_ie8: true,
          warnings: false
        },
        mangle: {
          screw_ie8: true
        },
        output: {
          comments: false,
          screw_ie8: true
        }
      }),
      new ExtractTextPlugin('[name].[contenthash:8].css')
    ]
  });
