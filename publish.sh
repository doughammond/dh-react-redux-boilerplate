#!/bin/sh

DS=`date +%y%m%d`
FN=boilerplate-modules-${DS}.tar.xz
yarn --flat
tar cvfJ ${FN} node_modules
scp ${FN} eclcs@eclcs.net:~/boilerplate-packages
