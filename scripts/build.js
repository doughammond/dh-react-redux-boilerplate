process.env.NODE_ENV = 'production';

const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const rimrafSync = require('rimraf').sync;
const gzipSize = require('gzip-size').sync;
const filesize = require('filesize');

const config = require('../config/webpack.config.prod');
const paths = require('../config/paths');

rimrafSync(paths.appBuild + '/*');

console.log('Creating an optimized production build...');
webpack(config).run(function(err, stats) {
  if (err) {
    console.error('Failed to create a production build. Reason:');
    console.error(err.message || err);
    process.exit(1);
  }

  console.log(chalk.green('Compiled successfully.'));
  console.log();

  console.log('File sizes after gzip:');
  console.log();
  const assets = stats.toJson().assets
    .filter(asset => /\.(js|css)$/.test(asset.name))
    .map(asset => {
      const fileContents = fs.readFileSync(paths.appBuild + '/' + asset.name);
      return {
        name: asset.name,
        size: gzipSize(fileContents)
      };
    });
  assets.sort((a, b) => b.size - a.size);
  assets.forEach(asset => {
    console.log(
      '  ' + chalk.dim('build' + path.sep) + chalk.cyan(asset.name) + ': ' +
      chalk.green(filesize(asset.size))
    );
  });
  console.log();

});
