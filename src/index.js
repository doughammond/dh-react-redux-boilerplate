// @flow

import React, { Component } from 'react';
import ReactDOM from 'react-dom';


class Test extends Component {
  render() {
    return <h1>Test</h1>;
  }
}

ReactDOM.render(
  <Test />,
  document.getElementById('root')
);
